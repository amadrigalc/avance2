package madrigal.adriana.bl.logic;

import madrigal.adriana.bl.entities.*;

import java.time.LocalDate;
import java.util.ArrayList;

public class UserGestor {



    public String registrarAdmin(String nombre, String apellido1, String apellido2, String identificacion, Direccion direccion, String usuario, String contrasenna){

        Admin admin = new Admin(nombre,apellido1,apellido2,identificacion,direccion,usuario,contrasenna);
        String resultado = "El Administrador" + admin.getIdentificacion() + "fue creado con exito";

        return resultado;
    }

    public String mostrarAdmin(){
        Admin admin = new Admin();
        String resultado = admin.toString();

        return resultado;

    }

    public String registrarUsuario(String nombre, String apellido1, String apellido2, String identificacion, Direccion direccion, String usuario, String  contrasenna, LocalDate fechaNac, int edad, String genero, String celular, boolean estado, MedioTransporte vehiculo){

        Usuario nuevoUsuario = new Usuario(nombre,apellido1,apellido2,identificacion,direccion,usuario,contrasenna,fechaNac,edad,genero,celular,estado,vehiculo);
        String resultado = "El usuario" + nuevoUsuario.getUsuario() + "fue creado con exito";

        return resultado;
    }

    public ArrayList<Usuario> listarUsuarios(){

        ArrayList tempLista = new ArrayList();


        return tempLista;

    }

    public String registrarConductor(String nombre, String apellido1, String apellido2, String identificacion, Direccion direccion, String usuario, String contrasenna, LocalDate fechaNac, int edad, boolean estado){

        Conductor nuevoConductor = new Conductor(nombre, apellido1, apellido2, identificacion, direccion, usuario, contrasenna,fechaNac,edad, estado);
        String resultado = "El conductor" + nuevoConductor.getUsuario() + "fue creado con exito";

        return resultado;

    }

    public ArrayList<Usuario> listarConductor(){

        ArrayList tempLista = new ArrayList();


        return tempLista;

    }




}
