package madrigal.adriana.bl.entities;

public class Admin extends Persona {
    public Admin() {
    }

    public Admin(String nombre, String apellido1, String apellido2, String identificacion, Direccion direccion, String usuario, String contrasenna) {
        super(nombre, apellido1, apellido2, identificacion, direccion, usuario, contrasenna);
    }

    @Override
    public String toString() {
        return "Admin{} " + super.toString();
    }

}
