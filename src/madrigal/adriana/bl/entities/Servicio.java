package madrigal.adriana.bl.entities;

import java.util.Objects;

public class Servicio {

    private double price;
    private String name;
    private String descripcion;

    public Servicio() {
    }

    public Servicio(double price, String name, String descripcion) {
        this.price = price;
        this.name = name;
        this.descripcion = descripcion;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public String toString() {
        return "Servicio{" +
                "price=" + price +
                ", name='" + name + '\'' +
                ", descripcion='" + descripcion + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Servicio servicio = (Servicio) o;
        return Double.compare(servicio.getPrice(), getPrice()) == 0 &&
                Objects.equals(getName(), servicio.getName()) &&
                Objects.equals(getDescripcion(), servicio.getDescripcion());
    }

}
