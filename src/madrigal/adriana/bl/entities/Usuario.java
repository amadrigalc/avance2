package madrigal.adriana.bl.entities;

import java.time.LocalDate;
import java.util.Objects;

public class Usuario extends Persona{
    private LocalDate fechaNac;
    private int edad;
    private String genero;
    private String celular;
    private boolean estado;
    private MedioTransporte vehiculo;

    public Usuario() {
    }

    public Usuario(String nombre, String apellido1, String apellido2, String identificacion, Direccion direccion, String usuario, String  contrasenna, LocalDate fechaNac, int edad, String genero, String celular, boolean estado, MedioTransporte vehiculo) {
        super(nombre, apellido1, apellido2, identificacion, direccion, usuario, contrasenna);
        this.fechaNac = fechaNac;
        this.edad = edad;
        this.genero = genero;
        this.celular = celular;
        this.estado = estado;
        this.vehiculo = vehiculo;
    }

    public LocalDate getFechaNac() {
        return fechaNac;
    }

    public void setFechaNac(LocalDate fechaNac) {
        this.fechaNac = fechaNac;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public boolean isEstado() {
        return estado;
    }

    public MedioTransporte getVehiculo() {
        return vehiculo;
    }

    public void setVehiculo(MedioTransporte vehiculo) {
        this.vehiculo = vehiculo;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    @Override
    public String toString() {
        return "Usuario{" + super.toString() +
                "fechaNac=" + fechaNac +
                ", edad=" + edad +
                ", genero='" + genero + '\'' +
                ", celular='" + celular + '\'' +
                ", estado=" + estado +
                ", vehiculo=" + vehiculo +
                "} " ;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Usuario usuario = (Usuario) o;
        return getEdad() == usuario.getEdad() &&
                isEstado() == usuario.isEstado() &&
                Objects.equals(getFechaNac(), usuario.getFechaNac()) &&
                Objects.equals(getGenero(), usuario.getGenero()) &&
                Objects.equals(getCelular(), usuario.getCelular()) &&
                Objects.equals(getVehiculo(), usuario.getVehiculo());
    }

}
