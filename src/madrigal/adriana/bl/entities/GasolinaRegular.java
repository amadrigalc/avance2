package madrigal.adriana.bl.entities;

public class GasolinaRegular extends Servicio{
    public GasolinaRegular() {
        //super();
        super.setPrice(800);
        super.setName("Gasolina Regular");
        super.setDescripcion("Servicio de envio gasolina regular por litro");
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
