package madrigal.adriana.bl.entities;

import java.util.Objects;

public class Persona {
    private String nombre;
    private String apellido1;
    private String apellido2;
    private String identificacion;
    private Direccion direccion;
    private String usuario;
    private String contrasenna;

    public Persona() {
    }

    public Persona(String nombre, String apellido1, String apellido2, String identificacion, Direccion direccion, String usuario, String contrasenna) {
        this.nombre = nombre;
        this.apellido1 = apellido1;
        this.apellido2 = apellido2;
        this.identificacion = identificacion;
        this.direccion = direccion;
        this.usuario = usuario;
        this.contrasenna = contrasenna;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido1() {
        return apellido1;
    }

    public void setApellido1(String apellido1) {
        this.apellido1 = apellido1;
    }

    public String getApellido2() {
        return apellido2;
    }

    public void setApellido2(String apellido2) {
        this.apellido2 = apellido2;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public Direccion getDireccion() {
        return direccion;
    }

    public void setDireccion(Direccion direccion) {
        this.direccion = direccion;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getContrasenna() {
        return contrasenna;
    }

    public void setContrasenna(String contrasenna) {
        this.contrasenna = contrasenna;
    }

    @Override
    public String toString() {
        return "nombre='" + nombre + '\'' +
                ", apellido1='" + apellido1 + '\'' +
                ", apellido2='" + apellido2 + '\'' +
                ", identificacion='" + identificacion + '\'' +
                ", direccion='" + direccion + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Persona persona = (Persona) o;
        return Objects.equals(nombre, persona.nombre) &&
                Objects.equals(apellido1, persona.apellido1) &&
                Objects.equals(apellido2, persona.apellido2) &&
                Objects.equals(identificacion, persona.identificacion) &&
                Objects.equals(direccion, persona.direccion) &&
                Objects.equals(usuario, persona.usuario) &&
                Objects.equals(contrasenna, persona.contrasenna);
    }
}
