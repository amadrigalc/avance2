package madrigal.adriana.bl.entities;

import java.time.LocalDate;

public class Conductor extends Persona{

    private LocalDate fechaNac;
    private int edad;
    private boolean estado;

    public Conductor() {
    }

    public Conductor(String nombre, String apellido1, String apellido2, String identificacion, Direccion direccion, String usuario, String contrasenna, LocalDate fechaNac, int edad, boolean estado) {
        super(nombre, apellido1, apellido2, identificacion, direccion, usuario, contrasenna);
        this.fechaNac = fechaNac;
        this.edad = edad;
        this.estado = estado;
    }

    public LocalDate getFechaNac() {
        return fechaNac;
    }

    public void setFechaNac(LocalDate fechaNac) {
        this.fechaNac = fechaNac;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    @Override
    public String toString() {
        return "Conductor{" + super.toString() +
                "fechaNac=" + fechaNac +
                ", edad=" + edad +
                ", estado=" + estado +
                "} " ;
    }
}
