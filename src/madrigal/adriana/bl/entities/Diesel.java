package madrigal.adriana.bl.entities;

public class Diesel extends Servicio{
    public Diesel() {
        //super();
        super.setPrice(500);
        super.setName("Diesel");
        super.setDescripcion("Servicio de envio diesel por litro");
    }

    @Override
    public String toString() {
        return "Diesel: " + super.toString();
    }
}
