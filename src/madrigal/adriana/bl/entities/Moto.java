package madrigal.adriana.bl.entities;

public class Moto extends MedioTransporte{

    private String tipo;

    public Moto() {
    }

    public Moto(String placa, String marca, String tipo) {
        super(placa, marca);
        this.tipo = tipo;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Override
    public String toString() {
        return "Moto{" +
                "tipo='" + tipo + '\'' +
                "} " + super.toString();
    }
}
