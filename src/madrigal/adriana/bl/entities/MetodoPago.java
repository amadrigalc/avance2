package madrigal.adriana.bl.entities;

import java.time.LocalDate;

public class MetodoPago {

    private String provedor;
    private String numeroTarjeta;
    private String cv;
    private LocalDate fechaNacimiento;

    public MetodoPago() {
    }

    public MetodoPago(String provedor, String numeroTarjeta, String cv, LocalDate fechaNacimiento) {
        this.provedor = provedor;
        this.numeroTarjeta = numeroTarjeta;
        this.cv = cv;
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getProvedor() {
        return provedor;
    }

    public void setProvedor(String provedor) {
        this.provedor = provedor;
    }

    public String getNumeroTarjeta() {
        return numeroTarjeta;
    }

    public void setNumeroTarjeta(String numeroTarjeta) {
        this.numeroTarjeta = numeroTarjeta;
    }

    public String getCv() {
        return cv;
    }

    public void setCv(String cv) {
        this.cv = cv;
    }

    public LocalDate getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(LocalDate fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    @Override
    public String toString() {
        return "MetodoPago{" +
                "provedor='" + provedor + '\'' +
                ", numeroTarjeta='" + numeroTarjeta + '\'' +
                ", cv='" + cv + '\'' +
                ", fechaNacimiento=" + fechaNacimiento +
                '}';
    }
}
