package madrigal.adriana.bl.entities;

public class Ubicacion {
    private Direccion direccion;

    public Ubicacion() {
    }

    public Ubicacion(Direccion direccion) {
        this.direccion = direccion;
    }

    public Direccion getDireccion() {
        return direccion;
    }

    public void setDireccion(Direccion direccion) {
        this.direccion = direccion;
    }

    @Override
    public String toString() {
        return "Ubicacion{" +
                "direccion=" + direccion +
                '}';
    }
}
