package madrigal.adriana.bl.entities;

public class MedioTransporte {
    private String placa;
    private String marca;

    public MedioTransporte() {
    }

    public MedioTransporte(String placa, String marca) {
        this.placa = placa;
        this.marca = marca;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    @Override
    public String toString() {
        return "MedioTransporte, placa es: '" + placa + ", la marca es: " + marca;
    }
}
