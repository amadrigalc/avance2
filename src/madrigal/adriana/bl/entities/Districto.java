package madrigal.adriana.bl.entities;

public class Districto {
    private String name;

    public Districto() {
    }

    public Districto(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Districto{" +
                "name='" + name + '\'' +
                '}';
    }
}
