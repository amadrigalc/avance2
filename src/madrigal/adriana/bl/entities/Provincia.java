package madrigal.adriana.bl.entities;

public class Provincia {
    private String name;

    public Provincia() {
    }

    public Provincia(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Provincia{" +
                "name='" + name + '\'' +
                '}';
    }
}
