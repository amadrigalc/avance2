package madrigal.adriana.bl.entities;

public class Pedido {
    private String pedidoID;
    private String fechaCreacion;
    private Servicio servicio;
    private Usuario cliente;
    private Boolean estatus;
    private MetodoPago metodoPago;
    private Conductor conductor;
    private double cargo;
    private Ubicacion ubicacion;

    public Pedido() {

    }

    public Pedido(String pedidoID, String fechaCreacion,Servicio servicio, Usuario cliente, Boolean estatus, MetodoPago metodoPago, Conductor conductor, Ubicacion ubicacion) {
        this.pedidoID = pedidoID;
        this.fechaCreacion = fechaCreacion;
        this.servicio = servicio;
        this.cliente = cliente;
        this.estatus = estatus;
        this.metodoPago = metodoPago;
        this.conductor = conductor;
        this.ubicacion = ubicacion;

    }

    public String getPedidoID() {
        return pedidoID;
    }

    public void setPedidoID(String pedidoID) {
        this.pedidoID = pedidoID;
    }

    public String getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(String fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Servicio getServicio() {
        return servicio;
    }

    public void setServicio(Servicio servicio) {
        this.servicio = servicio;
    }

    public Usuario getCliente() {
        return cliente;
    }

    public void setCliente(Usuario cliente) {
        this.cliente = cliente;
    }

    public Boolean getEstatus() {
        return estatus;
    }

    public void setEstatus(Boolean estatus) {
        this.estatus = estatus;
    }

    public MetodoPago getMetodoPago() {
        return metodoPago;
    }

    public void setMetodoPago(MetodoPago metodoPago) {
        this.metodoPago = metodoPago;
    }

    public Conductor getConductor() {
        return conductor;
    }

    public void setConductor(Conductor conductor) {
        this.conductor = conductor;
    }

    public double getCargo() {
        return cargo;
    }

    public void setCargo(double cargo) {
        this.cargo = cargo;
    }

    public Ubicacion getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(Ubicacion ubicacion) {
        this.ubicacion = ubicacion;
    }

    @Override
    public String toString() {
        return "Pedido{" +
                "pedidoID='" + pedidoID + '\'' +
                ", fechaCreacion='" + fechaCreacion + '\'' +
                ", cliente=" + cliente +
                ", estatus=" + estatus +
                ", metodoPago=" + metodoPago +
                ", conductor=" + conductor +
                '}';
    }
}
