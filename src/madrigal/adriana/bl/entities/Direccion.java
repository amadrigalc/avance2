package madrigal.adriana.bl.entities;

public class Direccion {
    private Provincia provincia;
    private Canton canton;
    private Districto districto;

    public Direccion() {
    }

    public Direccion(Provincia provincia, Canton canton, Districto districto) {
        this.provincia = provincia;
        this.canton = canton;
        this.districto = districto;
    }

    public Provincia getProvincia() {
        return provincia;
    }

    public void setProvincia(Provincia provincia) {
        this.provincia = provincia;
    }

    public Canton getCanton() {
        return canton;
    }

    public void setCanton(Canton canton) {
        this.canton = canton;
    }

    public Districto getDistricto() {
        return districto;
    }

    public void setDistricto(Districto districto) {
        this.districto = districto;
    }

    @Override
    public String toString() {
        return "Direccion{" +
                "provincia=" + provincia +
                ", canton=" + canton +
                ", districto=" + districto +
                '}';
    }
}
