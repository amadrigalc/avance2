package madrigal.adriana.bl.entities;

public class Automovil extends MedioTransporte {
    private String tipo;
    private String color;
    private String anho;

    public Automovil() {
    }

    public Automovil(String placa, String marca, String tipo, String color, String anho) {
        super(placa, marca);
        this.tipo = tipo;
        this.color = color;
        this.anho = anho;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getAnho() {
        return anho;
    }

    public void setAnho(String anho) {
        this.anho = anho;
    }

    @Override
    public String toString() {
        return  super.toString() + ", es tipo Automovil, tipo es: " + tipo +
                ", color es: " + color + ", el año es: " + anho;
    }
}
