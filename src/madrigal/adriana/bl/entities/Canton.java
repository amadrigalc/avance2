package madrigal.adriana.bl.entities;

public class Canton {
    private String name;

    public Canton() {
    }

    public Canton(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Canton{" +
                "name='" + name + '\'' +
                '}';
    }
}
