package madrigal.adriana.tl;

import madrigal.adriana.bl.entities.*;
import madrigal.adriana.bl.logic.UserGestor;
import madrigal.adriana.ui.UI;

import java.io.IOException;
import java.time.LocalDate;

public class Controlador {

    private UI interfaz = new UI();
    private UserGestor userGestor = new UserGestor();

    public void start() throws IOException {

        int opcion = -1;

        do {
            interfaz.showMenu();
            opcion = interfaz.leerOpcion();
            procesarOpcion(opcion);
        } while (opcion != 0);

    }

    private void procesarOpcion(int opcion) throws IOException {

        switch (opcion){
            case 1:
              registrarUsuario();
              break;
            case 2:
                registrarConductor();
                break;
            case 3:
                registrarAdmin();
                break;
            case 4:
                listarConductores();
                break;
            case 0:
                System.out.println("Gracias por usar nuestra aplicación");
             break;
            default:
                System.out.println("La opción ingresada es invalida");
        }

    }

    private void registrarUsuario() throws IOException {

        interfaz.imprimirMensaje("Ingrese el nombre de pila del usuario a registrar: ");
        String nombre = interfaz.leerTexto();
        interfaz.imprimirMensaje("Ingrese el primer apellido del usuario a registrar: ");
        String apellido1 = interfaz.leerTexto();
        interfaz.imprimirMensaje("Ingrese el segundo apellido del usuario a registrar: ");
        String apellido2 = interfaz.leerTexto();
        interfaz.imprimirMensaje("Ingrese la identificación del usuario a registrar: ");
        String identificacion = interfaz.leerTexto();
        interfaz.imprimirMensaje("Ingrese el canton de residencia del usuario a registrar: ");
        Districto districto = new Districto(interfaz.leerTexto());
        interfaz.imprimirMensaje("Ingrese el canton de residencia del usuario a registrar: ");
        Canton canton = new Canton(interfaz.leerTexto());
        interfaz.imprimirMensaje("Ingrese el canton de residencia del usuario a registrar: ");
        Provincia provincia = new Provincia(interfaz.leerTexto());
        Direccion direccion = new Direccion(provincia, canton, districto);
        interfaz.imprimirMensaje("Ingrese el nombre de usuario a registrar:");
        String usuario = interfaz.leerTexto();
        interfaz.imprimirMensaje("Ingrese la contraseña: ");
        String contrasenna = interfaz.leerTexto();
        interfaz.imprimirMensaje("Ingrese el año de nacimiento");
        int anho = interfaz.leerOpcion();
        interfaz.imprimirMensaje("Ingrese el mes de nacimiento");
        int mes = interfaz.leerOpcion();
        interfaz.imprimirMensaje("Ingrese el dia de nacimiento");
        int dia = interfaz.leerOpcion();
        LocalDate fechaNac = LocalDate.of(anho,mes,dia);
        interfaz.imprimirMensaje("Ingrese la edad del usuario:");
        int edad = interfaz.leerOpcion();
        interfaz.imprimirMensaje("Ingrese si es Femenino, Masculino o Indefinido");
        String genero = interfaz.leerTexto();
        interfaz.imprimirMensaje("Ingrese el numero de telefono");
        String celular = interfaz.leerTexto();
        boolean estado = true;
        interfaz.imprimirMensaje("ingrese el numero de placa: ");
        String placa = interfaz.leerTexto();
        interfaz.imprimirMensaje("ingrese la marca del vehiculo: ");
        String marca = interfaz.leerTexto();
        MedioTransporte vehiculo = new MedioTransporte(placa, marca);

        userGestor.registrarUsuario(nombre,apellido1,apellido2,identificacion,direccion,usuario,contrasenna,fechaNac,edad,genero,celular,estado,vehiculo);

    }

    private void registrarConductor() throws IOException {
        interfaz.imprimirMensaje("Ingrese el nombre de pila del conductor a registrar: ");
        String nombre = interfaz.leerTexto();
        interfaz.imprimirMensaje("Ingrese el primer apellido del conductor a registrar: ");
        String apellido1 = interfaz.leerTexto();
        interfaz.imprimirMensaje("Ingrese el segundo apellido del conductor a registrar: ");
        String apellido2 = interfaz.leerTexto();
        interfaz.imprimirMensaje("Ingrese la identificación del conductor a registrar: ");
        String identificacion = interfaz.leerTexto();
        interfaz.imprimirMensaje("Ingrese el canton de residencia del conductor a registrar: ");
        Districto districto = new Districto(interfaz.leerTexto());
        interfaz.imprimirMensaje("Ingrese el canton de residencia del conductor a registrar: ");
        Canton canton = new Canton(interfaz.leerTexto());
        interfaz.imprimirMensaje("Ingrese el canton de residencia del conductor a registrar: ");
        Provincia provincia = new Provincia(interfaz.leerTexto());
        Direccion direccion = new Direccion(provincia, canton, districto);
        interfaz.imprimirMensaje("Ingrese el nombre de conductor a registrar:");
        String usuario = interfaz.leerTexto();
        interfaz.imprimirMensaje("Ingrese la contraseña: ");
        String contrasenna = interfaz.leerTexto();
        interfaz.imprimirMensaje("Ingrese el año de nacimiento");
        int anho = interfaz.leerOpcion();
        interfaz.imprimirMensaje("Ingrese el mes de nacimiento");
        int mes = interfaz.leerOpcion();
        interfaz.imprimirMensaje("Ingrese el dia de nacimiento");
        int dia = interfaz.leerOpcion();
        LocalDate fechaNac = LocalDate.of(anho,mes,dia);
        interfaz.imprimirMensaje("Ingrese la edad del conductor:");
        int edad = interfaz.leerOpcion();
        interfaz.imprimirMensaje("Ingrese si es Femenino, Masculino o Indefinido");
        String genero = interfaz.leerTexto();
        interfaz.imprimirMensaje("Ingrese el numero de telefono");
        String celular = interfaz.leerTexto();
        boolean estado = true;

        userGestor.registrarConductor(nombre, apellido1, apellido2, identificacion, direccion, usuario, contrasenna,fechaNac,edad, estado);
    }

    private void registrarAdmin() throws IOException {

        interfaz.imprimirMensaje("Ingrese el nombre de pila del conductor a registrar: ");
        String nombre = interfaz.leerTexto();
        interfaz.imprimirMensaje("Ingrese el primer apellido del conductor a registrar: ");
        String apellido1 = interfaz.leerTexto();
        interfaz.imprimirMensaje("Ingrese el segundo apellido del conductor a registrar: ");
        String apellido2 = interfaz.leerTexto();
        interfaz.imprimirMensaje("Ingrese la identificación del conductor a registrar: ");
        String identificacion = interfaz.leerTexto();
        interfaz.imprimirMensaje("Ingrese el canton de residencia del conductor a registrar: ");
        Districto districto = new Districto(interfaz.leerTexto());
        interfaz.imprimirMensaje("Ingrese el canton de residencia del conductor a registrar: ");
        Canton canton = new Canton(interfaz.leerTexto());
        interfaz.imprimirMensaje("Ingrese el canton de residencia del conductor a registrar: ");
        Provincia provincia = new Provincia(interfaz.leerTexto());
        Direccion direccion = new Direccion(provincia, canton, districto);
        interfaz.imprimirMensaje("Ingrese el nombre de conductor a registrar:");
        String usuario = interfaz.leerTexto();
        interfaz.imprimirMensaje("Ingrese la contraseña: ");
        String contrasenna = interfaz.leerTexto();

        userGestor.registrarAdmin(nombre,apellido1,apellido2,identificacion,direccion,usuario,contrasenna);

    }

    private void listarConductores() {
        userGestor.listarConductor();

    }


}
